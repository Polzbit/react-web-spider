/* Web-Spider assignment created by bar polyak for ActiveFence interview */
import './App.css';
import CrawlerView from './components/CrawlerView';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h4>Web Spider</h4>
      </header>
      <CrawlerView></CrawlerView>
    </div>
  );
}

export default App;
